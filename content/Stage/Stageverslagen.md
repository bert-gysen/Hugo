+++
Categories = ["Stage"]
Description = ""
date = "2016-05-10T00:17:29+02:00"
menu = "main"
title = "Stageverslagen"

+++
### Dag 1
Eerst heb ik een rondleiding van mijn stagementor gekregen en stelde hij me aan iedereen voor. Daarna zijn we naar de kelder gegaan en heeft hij een korte uitleg over de servers gegeven. Ik kreeg en laptop die aangesloten werd op een monitor en toetsenbord om mee te werken. Daarmee heb ik hun netwerkstructuur in Visio getekend.

### Dag 2
Toen ik aankwam heb ik mij meteen achter mijn pc gezet en begon ik verder te werken aan de netwerkstructuur. In de loop van de dag ben ik bij mijn stagementor langs gegaan en heb ik al wat vragen van mijn GIP taak van bedrijfsbeheer kunnen oplossen. Daarna zijn we naar de verantwoordelijke voor de Financiële afdeling gegaan en heb ik een offerte en aankoopfactuur gekregen. Op het einde van de dag heb ik nog wat vragen gesteld in verband met hun netwerktool.

### Dag 3
Op de derde dag ben ik weer meteen aan de pc gaan zitten en ben verder gegaan met Visio. We zijn nog eens naar de servers gegaan waar ik foto’s van heb getrokken zodat ik die IP’s zeker al had. Zo heb ik mijn netwerkstructuur nog meer in detail genomen. Op het einde hebben we weer even overlopen waar ik stond.

## Dag 4
Dag vier was bijna net hetzelfde als dag drie. Ik ben weer verdergegaan met Visio. IK heb de printers en kopieerapparaten in kaart gebracht. Ik heb ook hun tool waarmee ze hun virtuele servers beheren bekeken. Mijn stagementor en ik hebben ook even de software die het bedrijf gebruikt overlopen zoals KPD.

### Dag 5
Vandaag kwam de leverancier van Kaseya langs. Mijn stagementor vroeg of ik mee wou gaan luisteren en ik stemde in. Terwijl de leverancier ons de nieuwe opties van de tool liet zien viel de telenet lijn weg. De Belgacom lijn had normaal dan moeten overpakken maar dat deed hij niet, dus hebben we de telenet router helemaal ontkoppeld en toen werkte het wel. Daarna ben ik verdergegaan met Visio, ik ben toen begonnen met de softwarepaketten in kaart te brengen.

### Dag 6
Eerst en vooral had ik een licentieprobleem met visio, maar dat was snel opgelost. Daarna kon ik verdergaan met al de softwarepaketten in kaart te brengen. Ik ben in de namiddag begonnen met hun security systeem eens te bekijken. Ik heb dan gewerkt met hun programma waar ze de camera’s mee besturen en ook hun tool waarmee ze de badges van de medewerkers instellen.

### Dag 7
Vandaag zijn we naar REGA gegaan, het zusterbedrijf van DCA. Daar heb ik weer het netwerk bekeken. Het schema was best snel klaar omdat ik het al eens had gedaan bij DCA, dus ben ik verdergegaan met het domoticasysteem daar. Ik heb gezien hoe ze met Ipads het licht en de radio konden bedienen en heb geholpen met de nieuwe pc van de projectleider te plaatsen.

### Dag 8
Ik had erg veel zin in deze dag, vooral omdat ik vandaag eindelijk tegenover mijn stagementor kon gaan zitten. Ik heb mijn netwerkschema dan ook helemaal kunnen afwerken omdat ik constant vragen kon stellen. Daarna heb ik wat geholpen met hun nieuwe office 365 licentie op hun clients te zetten en hebbben we een nieuwe printer geïnstalleerd.

### Dag 9
Vandaag kon ik weer tegenover mij stagementor zitten. Ik heb me vandaag vooral beziggehouden met Kaseya, hun netwerktool, omdat ik daar mijn casestudy over doe. Ik heb heel hun tool onderzocht en heb erg veel vragen kunnen stellen. Daarna heb ik meegeholpen aan een script voor een standaardprinter in te stellen. Ook heb ik me beziggehouden met foto’s te nemen zoals bijvoorbeeld in het serverlokaal.

### Dag 10
De laatste dag heb ik samen met mijn mentor mijn volledige bundel van hun netwerk overlopen en nog wat aanpassingen gemaakt. Daarna kreeg ik de opdracht om oude laptops te testen en de harde schijf er uit te halen als ze niet meer werkten.