+++
Categories = ["Engels", "Systeembeheer"]
Description = ""
date = "2015-10-15T00:14:46+02:00"
menu = "main"
title = "IT-tapa instructie video"

+++
### Trello Tutorials

- Nederlands filmpje: <br>
<iframe width="75%" height="330" src="http://www.youtube.com/embed/vtrBZNaJU_c" frameborder="0" allowfullscreen></iframe>

- Engels filmpje: <br>
<iframe width="75%" height="330" src="http://www.youtube.com/embed/cej_UYiT3Mk" frameborder="0" allowfullscreen>

