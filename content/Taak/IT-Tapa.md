+++
Categories = ["Systeembeheer"]
Description = ""
date = "2015-10-15T00:03:02+02:00"
menu = "main"
title = "IT Tapa (sessies)"

+++

### IT-tapa

Ik heb mijn IT-tapa presentatie samen met Joeri Sips gedaan. Wij hebben samen gekozen om <a href="https://trello.com/">Trello</a> als ons onderwerp te nemen. Trello is een online tool om takenlijsten mee te maken. Wij zijn vorig jaar voor het eerst in contact gekomen met Trello en vonden het een handige tool om voor jezelf de takenlijst te maken die het best bij jou past. 

<a href="http://joeri-sips.github.io/">Joeri's Site</a> <br>
<a href="https://www.facebook.com/IT-tapa-588405757863108/?fref=ts">IT-tapa facebookpagina</a> <br>
<img src="https://scontent-bru2-1.xx.fbcdn.net/hphotos-xpa1/t31.0-8/12238016_931268803576800_4718562916260969016_o.jpg"></img>
<img src="https://scontent-bru2-1.xx.fbcdn.net/hphotos-xpf1/t31.0-8/12248194_931268796910134_4580047370603038564_o.jpg"></img>