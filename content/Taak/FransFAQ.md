+++
Categories = ["Frans"]
Description = ""
date = "2015-10-15T00:09:43+02:00"
menu = "main"
title = "Pour la FAQ au sujet de Trello, cliquez ICI."

+++

### Foire Aux Questions
Vous avez des questions sur <a href="https://trello.com/">Trello</a>? Voici les réponses!

**On peut enlever un groupe?** <br>
Pas encore.


**On peut changer une liste de contrôle dans une carte?**  <br>
Oui, faites une liste de contrôle et cliquez sur “Convert to card”.


**Est-ce qu’il y a une limite de téléchargement?**  <br>
Oui, 10 mégaoctet par fichiers.


**On peut enlever un fichier?**  <br>
Non, vous pouvez seulement archiver les items.

**Comment est-ce que je cherche quelque chose?**  <br>
Utilisez le zone de texte en haut de la page.

**Est-ce qu’il y a une version payante?**  <br>
Oui, Trello Business Class est une version de Trello avec plus d’options.



