+++
Categories = ["Systeembeheer"]
Description = ""
date = "2016-04-10T00:12:25+02:00"
menu = "main"
title = "Case Study"

+++

### Kaseya

Kaseya is een netwerktool gebruikt door netwerkbeheerders om hun leven iets makkelijker en eenvoudiger te maken. De tool bevat allerlei functies zoals remote control, monitoring, dashboards, patch management, enzovoort.

Ikzelf vond kaseya in het begin best moeilijk te gebruiken. Ik werd overweldigd door alle opties die je had, maar als je de basis hebt is het een zeer nuttige tool!

Ik heb veel moeite gehad in het begin maar uiteindelijk was ik er wel mee weg. De tool heeft veel potentieel maar is nog niet echt af, er is een optie om alle apparaten in het netwerk te tonen maar ze worden niet allemaal gevonden. De tool ziet firewalls bijvoorbeeld aan als gateways dus die worden niet getoond.

<img src="../../Kaseya1.png" alt=""></img>
<img src="../../Kaseya2.png" alt=""></img>
<img src="../../Kaseya3.png" alt=""></img>