+++
date = "2015-10-22T16:38:15+02:00"
Categories= ["Toegepaste Informatica"]
title = "GIP taak 07"

+++
# Links

- <a href="https://gitlab.com/bert-gysen/Hugo.git">Gitlab</a>
- <a href="https://github.com/Bert-Gysen/localhost:1313">Github</a>
- <a href="http://localhost:1313/">Github pages</a>


# Vergelijking Hugo en Wordpress
Hugo:

- Maakt gebruikt van Markdown-bestandjes
- Is geschreven in GO
- Posts kunnen toevoegen offline
- Maakt gebruik van thema's

Wordpress:

- Is geschreven in PHP
- Geen Markdown-bestandjes
- Geen posts toevoegen online
- Geen gebruik van thema's

Ik vind Hugo een redelijke tool om website's te maken. Het was op het eerste zicht wat veel om in één keer op te nemen, maar uiteindelijk valt het allemaal wel mee. De thema's zijn ook erg handig en geven je de middelen om direct van start te gaan.

# Directory structuur

### Content map

In mijn content map zitten nog twee maps genaamd "taak" en "stage" daar ga ik mijn taken en stageverslagen opslaan.

### Layouts map

Hier zitten de bestanden die ik gekopiëerd heb uit het 'Hyde' thema. Ik heb wat aan de HTML files veranderd om bijvoorbeeld mijn posts op mijn homepage weg te doen.

### Static map

In mijn static map zitten mijn css-bestanden. Ik heb hier de achtergronden voor mijn sidebar, index en posts aangepast. Ik heb ook een nieuw favicon dat een letter 'B' is.


